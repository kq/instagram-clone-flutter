import 'package:flutter/material.dart';
import 'package:instagram_clone_flutter/pages/search_page.dart';
import 'package:material_symbols_icons/symbols.dart';

class FooterBar extends StatefulWidget {
  const FooterBar({super.key});

  @override
  State<FooterBar> createState() => _FooterBarState();
}

class _FooterBarState extends State<FooterBar> {
  int _selectedIndex = 1;
  void _navigateBottomNavbar(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<Widget> _children = [
    const Center(child: Text('Home')),
    const SearchPage(),
    const Center(child: Text('Reels')),
    const Center(child: Text('Likes')),
    const Center(child: Text('Account')),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: _navigateBottomNavbar,
        iconSize: 30.0,
        selectedItemColor: Colors.green[400],
        selectedLabelStyle: const TextStyle(fontSize: 0),
        unselectedLabelStyle: const TextStyle(fontSize: 0),
        currentIndex: _selectedIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Symbols.home),
            label: 'home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Symbols.search),
            label: 'search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Symbols.add_box),
            label: 'reels',
          ),
          BottomNavigationBarItem(
            icon: Icon(Symbols.favorite),
            label: 'likes',
          ),
          BottomNavigationBarItem(
            icon: Icon(Symbols.person),
            label: 'account',
          ),
        ],
      ),
    );
  }
}
